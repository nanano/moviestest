class CastList {
  List<Cast> items = new List();
  CastList();

  CastList.fromJsonList(List<dynamic> resp) {
    if (resp == null) return;
    resp.forEach((item) {
      final actor = Cast.fromJsonMap(item);
      items.add(actor);
    });
  }
}

class Cast {
  int castId;
  String character;
  String creditId;
  int gender;
  int id;
  String name;
  int order;
  String profilePath;

  Cast({
    this.castId,
    this.character,
    this.creditId,
    this.gender,
    this.id,
    this.name,
    this.order,
    this.profilePath,
  });

  //mapeo
  Cast.fromJsonMap(Map<String, dynamic> json) {
    castId = json['cast_id'];
    character = json['character'];
    creditId = json['credit_id'];
    gender = json['gender'];
    id = json['id'];
    name = json['name'];
    order = json['order'];
    profilePath = json['profile_path'];
  }

  getCastImage() {
    if (profilePath == null) {
      return 'https://upload.wikimedia.org/wikipedia/commons/6/64/Poster_not_available.jpg';
    }
    return 'https://image.tmdb.org/t/p/w500/$profilePath';
  }
}
