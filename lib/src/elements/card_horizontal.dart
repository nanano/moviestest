import 'package:flutter/material.dart';
import 'package:mov_app/src/models/movies_model.dart';
import 'package:mov_app/src/providers/movies_provider.dart';

class CardHorizontal extends StatelessWidget {
  CardHorizontal({@required this.cardList, @required this.nextPage});
  final List<Movie> cardList;
  final Function nextPage;

  final _pagecontroller =
      new PageController(initialPage: 1, viewportFraction: 0.3);

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    _pagecontroller.addListener(() {
      if (_pagecontroller.position.pixels >=
          _pagecontroller.position.maxScrollExtent - 200) {
        nextPage();
      }
    });
    return Container(
      child: Container(
        height: _screenSize.height * 0.2,
        child: PageView.builder(
          pageSnapping: false,
          controller: _pagecontroller,
          itemCount: cardList.length,
          itemBuilder: (context, i) => _createCard(context, cardList[i]),
        ),
      ),
    );
  }

  Widget _createCard(BuildContext context, Movie item) {
    item.uniqueId = '${item.id}-S';
    final movieCard = Container(
      margin: EdgeInsets.only(right: 15.0),
      child: Column(
        children: <Widget>[
          Hero(
            tag: item.uniqueId,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: FadeInImage(
                image: NetworkImage(item.getPosterImage()),
                placeholder: AssetImage('assets/noimg.png'),
                fit: BoxFit.cover,
                height: 125.0,
              ),
            ),
          ),
          SizedBox(height: 5.0),
          Text(
            item.title,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
    );

    return GestureDetector(
      child: movieCard,
      onTap: () {
        Navigator.pushNamed(context, 'detail', arguments: item);
      },
    );
  }
}
