import 'package:flutter/material.dart';
import 'package:mov_app/src/pages/home_page.dart';
import 'package:mov_app/src/pages/movie_detail.dart';

Map<String, WidgetBuilder> getRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => HomePage(),
    'detail': (BuildContext context) => MovieDetail(),
  };
}
