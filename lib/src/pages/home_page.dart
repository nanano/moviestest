import 'package:flutter/material.dart';
import 'package:mov_app/src/elements/card_horizontal.dart';
import 'package:mov_app/src/elements/card_swipper.dart';
import 'package:mov_app/src/providers/movies_provider.dart';
import 'package:mov_app/src/search/search_delegate.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key}) : super(key: key);
  final MoviesProvider moviesProvider = new MoviesProvider();
  @override
  Widget build(BuildContext context) {
    moviesProvider.getGetPopular();
    return Container(
        child: Scaffold(
      appBar: AppBar(
        title: Text('Peliculas', style: TextStyle(color: Colors.blueGrey)),
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: <Widget>[
          IconButton(
            color: Colors.blueGrey,
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(context: context, delegate: DataSearch());
            },
          )
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[_cardSwipper(), _popular(context)],
      ),
    ));
  }

  Widget _cardSwipper() {
    return FutureBuilder(
      future: moviesProvider.getNowPlaying(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return CardSwipper(
            cardList: snapshot.data,
          );
        } else {
          return Container(
              height: 400, child: Center(child: CircularProgressIndicator()));
        }
      },
    );
  }

  Widget _popular(BuildContext context) {
    return Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(left: 30.0),
                child: Text('Populares',
                    style: Theme.of(context).textTheme.subhead)),
            SizedBox(height: 10.0),
            StreamBuilder(
              stream: moviesProvider.popularStream,
              builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                if (snapshot.hasData) {
                  return CardHorizontal(
                    cardList: snapshot.data,
                    nextPage: moviesProvider.getGetPopular,
                  );
                } else {
                  return Container(
                      height: 50,
                      child: Center(child: CircularProgressIndicator()));
                }
              },
            ),
          ],
        ));
  }
}
