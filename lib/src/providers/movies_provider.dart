import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mov_app/src/models/actors_model.dart';
import 'package:mov_app/src/models/movies_model.dart';

class MoviesProvider {
  String _apykey = '4d1a874629b664d36176260159cf2769';
  String _url = 'api.themoviedb.org';
  String _lenguage = 'en-US';

  //POPULAR STREAM
  int _popularPage = 0;
  bool _loading = false;
  List<Movie> _popularList = new List();
  final _popularStreamController = StreamController<
      List<
          Movie>>.broadcast(); //Broadcast tener muchos lugares que oigan el stream

  Function(List<Movie>) get popularSink => _popularStreamController.sink.add;

  Stream<List<Movie>> get popularStream => _popularStreamController.stream;

  void disposeStreams() {
    _popularStreamController?.close();
  }

  Future<List<Movie>> _processResponse(Uri url) async {
    final response = await http.get(url);

    final decodedData = json.decode(response.body);

    final movies = new Movies.fromJsonList(decodedData['results']);

    return movies.items;
  }

  Future<List<Movie>> getNowPlaying() async {
    final url = Uri.https(_url, '/3/movie/now_playing',
        {'api_key': _apykey, 'lenguage': _lenguage});

    return await this._processResponse(url);
  }

  Future<List<Movie>> getGetPopular() async {
    if (_loading) {
      return null;
    }
    _loading = true;
    _popularPage++;
    final url = Uri.https(_url, '/3/movie/popular', {
      'api_key': _apykey,
      'lenguage': _lenguage,
      'page': _popularPage.toString()
    });

    final response = await this._processResponse(url);
    _popularList.addAll(response);
    popularSink(_popularList);

    _loading = false;
    return response;
  }

  Future<List<Cast>> getActors(String movieId) async {
    final url = Uri.https(_url, '/3/movie/${movieId}/credits',
        {'api_key': _apykey, 'lenguage': _lenguage});

    final response = await http.get(url);

    final decodedData = json.decode(response.body);

    final cast = new CastList.fromJsonList(decodedData['cast']);

    return cast.items;
  }

  Future<List<Movie>> searchMovie(String query) async {
    final url = Uri.https(_url, '/3/search/movie',
        {'api_key': _apykey, 'lenguage': _lenguage, 'query': query});

    return await this._processResponse(url);
  }
}
