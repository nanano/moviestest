import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:mov_app/src/models/movies_model.dart';

class CardSwipper extends StatelessWidget {
  final List<Movie> cardList;

  const CardSwipper({@required this.cardList});

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.only(top: 30.0),
      width: _screenSize.width,
      height: _screenSize.height * 0.6,
      child: Swiper(
        itemWidth: _screenSize.width,
        itemHeight: _screenSize.height * 0.5,
        itemBuilder: (BuildContext context, int index) {
          cardList[index].uniqueId = '${cardList[index].id}-B';
          return Hero(
            tag: cardList[index].uniqueId,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: _posterImage(context, cardList[index])),
          );
        },
        itemCount: cardList.length,
        viewportFraction: 0.8,
        scale: 0.9,
      ),
    );
  }

  Widget _posterImage(BuildContext context, Movie movie) {
    final item = FadeInImage(
      fit: BoxFit.cover,
      image: NetworkImage(movie.getPosterImage()),
      placeholder: AssetImage('assets/noimg.png'),
    );
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, 'detail', arguments: movie);
      },
      child: item,
    );
  }
}
